﻿using System;
using UnityEngine;

namespace UnityStandardAssets.ImageEffects
{
	[RequireComponent(typeof (Camera))]
	public class FirstTryScript : PostEffectsBase
	{
		public ComputeShader minMaxReduceTextureCS;

		private RenderTexture renderTexture;
		private System.Diagnostics.Stopwatch stopWatch;
		private ComputeBuffer intermediateFloatBuffer;

		private const int NUM_THREADS_X = 32;
		private const int NUM_THREADS_Y = NUM_THREADS_X;
		private const int DO_MIN = 0; // 1 or 0

		public override bool CheckResources()
		{
			CheckSupport(false, true); // Do not need depth, do need hdr

			stopWatch = new System.Diagnostics.Stopwatch();

			if (!isSupported) { ReportAutoDisable (); }
			return isSupported;
		}
		
		private void OnRenderImage(RenderTexture source, RenderTexture destination)
		{
			renderTexture = new RenderTexture (source.width, source.height, 0);
			renderTexture.enableRandomWrite = true;
			renderTexture.Create ();

			int numGroupsX = (int)Mathf.Ceil ((float)source.width / NUM_THREADS_X);
			int numGroupsY = (int)Mathf.Ceil ((float)source.height / NUM_THREADS_X);
			int numGroups = numGroupsX * numGroupsY;

			intermediateFloatBuffer = new ComputeBuffer(numGroups, 4); // Second argument is size of elements in bytes

			minMaxReduceTextureCS.SetInt ("numCols", source.width);
			minMaxReduceTextureCS.SetInt ("numRows", source.height);
			minMaxReduceTextureCS.SetInt ("numGroupsX", numGroupsX);
			minMaxReduceTextureCS.SetInt ("numGroupsY", numGroupsY);
			minMaxReduceTextureCS.SetInt ("doMin", DO_MIN);

			minMaxReduceTextureCS.SetTexture (1, "outTexture", renderTexture);
			minMaxReduceTextureCS.SetTexture (0, "inTexture", source);
			minMaxReduceTextureCS.SetBuffer (0, "outBuffer", intermediateFloatBuffer);
			minMaxReduceTextureCS.SetBuffer (1, "outBuffer", intermediateFloatBuffer);

			stopWatch.Start();
			// +++ DISPATCH +++
			// First argument determines the kernel to dispatch
			minMaxReduceTextureCS.Dispatch (0, numGroupsX, numGroupsY, 1); // CSMain
			minMaxReduceTextureCS.Dispatch (1, 2, 1, 1);				   // CSSecond
			stopWatch.Stop();
			
			float[] readData = new float[numGroupsX * numGroupsY];
			intermediateFloatBuffer.GetData(readData);
			float result = -1;
/* silent unreachable Code warning
			if (numGroups < 1024 ||
				(DO_MIN == 1 && readData[0] < readData[1024]) ||
				(DO_MIN == 0 && readData[0] > readData[1024])) // TODO: At the moment this statement only supports up to Full HD resolution
			{
				result = readData[0];
			}
			else
			{
				result = readData[1024];
			}
*/
 			Debug.Log(((float)stopWatch.ElapsedTicks * 1000) / (float)System.Diagnostics.Stopwatch.Frequency);
			Debug.Log("Shader result: " + result);
			stopWatch.Reset();

			intermediateFloatBuffer.Dispose();

			Graphics.Blit (renderTexture, destination);
		}

		public void OnDestroy()
		{
			//intermediateFloatBuffer = null;
		}
	}
}
