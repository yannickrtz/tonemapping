﻿using UnityEngine;
using UnityEditor;
using UnityStandardAssets.ImageEffects;

[CustomEditor(typeof(TonemapperDebug))]
class TonemapperDebugEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		if(GUILayout.Button("Fully adapt"))
		{
			Debug.Log("Fully adapt.");
			Camera.main.GetComponent<TonemapperDebug>().fullyAdapt();
		}
		if(GUILayout.Button("Adapt 2 seconds"))
		{
			Debug.Log("adapt 2s.");
			Camera.main.GetComponent<TonemapperDebug>().updateAdaptationVariables(-0.71f,2);
		}
		if(GUILayout.Button("Adapt 300 seconds"))
		{
			Debug.Log("adapt 300s.");
			Camera.main.GetComponent<TonemapperDebug>().updateAdaptationVariables(-0.71f,300);
		}
	}
}