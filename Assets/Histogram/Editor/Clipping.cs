using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Clipping : EditorWindow {
	
	public Texture2D preview;
	public int width;
	public int height;
	public float left;
	public float top;
	
	[MenuItem("Window/Histogram Clipping Preview")]
	static void Init (){
		
		Clipping histogramPreviewWindow = (Clipping)EditorWindow.GetWindow (typeof(Clipping));

        histogramPreviewWindow.autoRepaintOnSceneChange = true;
        histogramPreviewWindow.Show();

    }
	
	public void OnGUI(){
		
		preview = Histogram.clippingTexture;
		
		if(preview){
			
			GUI.DrawTexture(new Rect(left,top,width,height) , preview);
			
		}
		
		
	}
	
	public void Update(){
		
		width = Histogram.width;
		height = Histogram.height;
		left = (position.width / 2) - (width / 2);
		top = (position.height / 2) - (height / 2);
		
		
	}
	
}
	
	