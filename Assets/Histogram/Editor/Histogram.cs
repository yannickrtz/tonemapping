
using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityStandardAssets.ImageEffects;

public class Histogram : EditorWindow {
	
	public enum Mode {
		
		Brightness,
		Luminosity,
		Red,
		Blue,
		Green
		
    };

	private int[] histogramMessage1;
	
	public static Rect[] previewResolutions;

	public static Mode histogramMode;
	
	public static bool doRed = false;
	public static bool doGreen = false;
	public static bool doBlue = false;
	public static bool showClipping = false;
	public static bool use32;

	public static int width;
	public static int height;
	public static int red;
	public static int blue;
	public static int green;
	public static int redLum;
	public static int greenLum;
	public static int blueLum;
	public static int histogramWidth = 255;
	public static int histogramHeight = 125;
	public static int menuInt;
	public static int resolutionInt;
	public static int mainCameraInt;
	public static int heightDivider = 8;
	public static int normalizeDivider = 10;
	public static int divider = 2;

	public static int[] brightness;
	public static int[] luminosity;
	public static int[] redChannel;
	public static int[] greenChannel;
	public static int[] blueChannel;

	public static Texture2D iconBrightness;
	public static Texture2D iconLuminosity;
	public static Texture2D iconRed;
	public static Texture2D iconGreen;
	public static Texture2D iconBlue;
	public static Texture2D iconCamera;
	public static Texture2D textureDivider;
	public static Texture2D iconResolutions;

	public static Texture2D screenTexture;
	public static Texture2D previewTexture;
	public static Texture2D histogramTexture;
	public static Texture2D clippingTexture;

	public static Color32 colorClippingWhite;
	public static Color32 colorClippingBlack;

	public static Color32[] textureColors;
	public static Color32[] histogramBuffer;
	

	public static RenderTexture renderTexture;

	public static Camera mainCamera;
	
	[MenuItem("Window/Histogram")]
	static void Init (){
		
		Histogram window = (Histogram)EditorWindow.GetWindow (typeof(Histogram));

		textureDivider = (Texture2D)Resources.Load("divider", typeof(Texture2D));
		
        window.autoRepaintOnSceneChange = true;
		
        window.Show();

    }
	
	public void initResolutions(){
	
		previewResolutions = new Rect[]{
			
			new Rect(0,0,800,600) ,
			new Rect(0,0,800,480) , 
			new Rect(0,0,640,480) , 
			new Rect(0,0,512,512) , 
			new Rect(0,0,320,240) ,
			new Rect(0,0,1000,400),
			new Rect(0,0,1800,900)
		
		};
/* Unreachable Code:
		if(width == null){
			
			width = 640;
			height = 480;
			
		}
*/
		
	}

	public void initTextures(){
		
		renderTexture 		= new RenderTexture(width ,height ,24);

		histogramTexture 	= new Texture2D(histogramWidth,histogramHeight, TextureFormat.ARGB32,false);
		screenTexture 		= new Texture2D(width ,height , TextureFormat.ARGB32,false);
		clippingTexture 	= new Texture2D(width ,height , TextureFormat.ARGB32,false);

		textureColors 		= new Color32[width  * height ];
		histogramBuffer 	= new Color32[histogramWidth * histogramHeight];
		
		brightness 			= new int[histogramWidth + 1];
		luminosity 			= new int[histogramWidth + 1];
		redChannel 			= new int[histogramWidth + 1];
		greenChannel 		= new int[histogramWidth + 1];
		blueChannel 		= new int[histogramWidth + 1];

		textureDivider = (Texture2D)Resources.Load("divider", typeof(Texture2D));
		
		Debug.Log(Application.unityVersion);
		

}
		
	public void Update (){
		
		if(EditorApplication.isPlaying){
		
			setMode(menuInt);
			
			grabScreen();
			
		} else if( mainCamera){

			if(mainCamera.targetTexture != null){

				mainCamera.targetTexture = null;

			}

		}
	
	}
		
	public void OnGUI(){  

		EditorGUILayout.Space();
		
		EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(260));

		GUIContent[] cameras = new GUIContent[Camera.allCameras.Length];

		for(int i = 0 ; i < Camera.allCameras.Length ; i++){
			
			cameras[i] = new GUIContent(Camera.allCameras[i].name, iconCamera);

		}
		
		GUILayout.Label("Camera to Use:");

		mainCameraInt = EditorGUILayout.Popup(mainCameraInt,cameras);

		mainCamera = Camera.allCameras[mainCameraInt];
		
		EditorGUILayout.EndHorizontal();
		
		//////////////////////////////////////////////////////////////////
		
		EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(255));

		if(previewResolutions == null){initResolutions();}
		
		GUIContent[] resolutionGUIContent = new GUIContent[previewResolutions.Length];

		for(int j = 0 ; j < previewResolutions.Length ; j++){
			
			resolutionGUIContent[j] = new GUIContent(previewResolutions[j].width.ToString() + "x" + previewResolutions[j].height.ToString(), iconResolutions);

		}
		
		GUILayout.Label("Clipping Resolution:");

		resolutionInt = EditorGUILayout.Popup(resolutionInt,resolutionGUIContent);

		width = (int)previewResolutions[resolutionInt].width;
		height = (int)previewResolutions[resolutionInt].height;
		
		if (GUI.changed){
		
			initTextures();
	        EditorPrefs.SetInt("clippingResolution", resolutionInt);
			
		}
		
		resolutionInt = EditorPrefs.GetInt("clippingResolution");
		
		EditorGUILayout.EndHorizontal();
		
		//////////////////////////////////////////////////////////////////
		
		doDivider();
		
		EditorGUILayout.BeginHorizontal();

		GUIContent[] toolbarOptions = new GUIContent[5];
		toolbarOptions[0] = new GUIContent("RGB", iconBrightness);
		toolbarOptions[1] = new GUIContent("Lum", iconLuminosity);
		toolbarOptions[2] = new GUIContent("Red", iconRed);
		toolbarOptions[3] = new GUIContent("Green", iconGreen);
		toolbarOptions[4] = new GUIContent("Blue", iconBlue);

		menuInt = GUILayout.Toolbar(menuInt, toolbarOptions , GUILayout.MaxWidth(260));
		
		EditorGUILayout.EndHorizontal();
		
		//////////////////////////////////////////////////////////////////
		
		EditorGUILayout.BeginHorizontal();

		colorClippingBlack = EditorGUILayout.ColorField("Black clip:" , colorClippingBlack , GUILayout.MaxWidth(260));
			
		EditorGUILayout.EndHorizontal();

		if (GUI.changed){

	        EditorPrefs.SetInt("colorClippingBlackR", colorClippingBlack.r);
	        EditorPrefs.SetInt("colorClippingBlackG", colorClippingBlack.g);
	        EditorPrefs.SetInt("colorClippingBlackB", colorClippingBlack.b);
	        EditorPrefs.SetInt("colorClippingBlackA", colorClippingBlack.a);

		}

		EditorGUILayout.BeginHorizontal();

			colorClippingWhite = EditorGUILayout.ColorField("White clip:" , colorClippingWhite , GUILayout.MaxWidth(260));

		EditorGUILayout.EndHorizontal();

		if (GUI.changed){

	        EditorPrefs.SetInt("colorClippingWhiteR", colorClippingWhite.r);
	        EditorPrefs.SetInt("colorClippingWhiteG", colorClippingWhite.g);
	        EditorPrefs.SetInt("colorClippingWhiteB", colorClippingWhite.b);
	        EditorPrefs.SetInt("colorClippingWhiteA", colorClippingWhite.a);

		}

        colorClippingBlack.r = (byte)EditorPrefs.GetInt("colorClippingBlackR");
        colorClippingBlack.g = (byte)EditorPrefs.GetInt("colorClippingBlackG");
        colorClippingBlack.b = (byte)EditorPrefs.GetInt("colorClippingBlackB");
        colorClippingBlack.a = (byte)EditorPrefs.GetInt("colorClippingBlackA");

        colorClippingWhite.r = (byte)EditorPrefs.GetInt("colorClippingWhiteR");
        colorClippingWhite.g = (byte)EditorPrefs.GetInt("colorClippingWhiteG");
        colorClippingWhite.b = (byte)EditorPrefs.GetInt("colorClippingWhiteB");
        colorClippingWhite.a = (byte)EditorPrefs.GetInt("colorClippingWhiteA");


		if(EditorApplication.isPlaying){
			
		//	GUILayout.DrawTexture( histogramTexture);
		if(histogramTexture)GUI.DrawTexture(new Rect(5, 150, histogramWidth, histogramHeight), histogramTexture, ScaleMode.ScaleToFit, true, 0);

		//	GUILayout.Box( clippingTexture );
			
		} // if isRunning?
					
	} 

	public void doDivider(){

		EditorGUILayout.BeginHorizontal();

		GUILayout.Label(textureDivider);

		EditorGUILayout.EndHorizontal();

	}
	
	public void doHistogram(int[] values){

		for(int x = 0 ; x < histogramWidth ; x++){
			
			for(int y = 0 ; y < histogramHeight ; y++){
				
				int pixelIndex =  y * histogramWidth + x;

				if(y <= values[x]){
					
					if(doRed){
					
						histogramBuffer[pixelIndex] = Color.red;
					
					}else if(doGreen){
					
						histogramBuffer[pixelIndex] = Color.green;
					
					}else if(doBlue){
					
						histogramBuffer[pixelIndex] = Color.blue;
					
					}else{
					
						histogramBuffer[pixelIndex] = Color.white;
					
					}
					
				} else{
					
					histogramBuffer[pixelIndex].a = 0;
				
				}
				
			}
			
		}
		
		histogramTexture.SetPixels32(histogramBuffer);
		histogramTexture.Apply();
		
	}
	
	public void initBuffer(int[] buffer){

		for(int z = 0 ; z < buffer.Length ; z++){
							
			buffer[z] = 0;
			
		}

	}
	
	public void normalizeBuffer(int[] buffer){
				
		int multiplier = textureColors.Length / 100000 + 1;

		for(int z = 0 ; z < buffer.Length ; z++){
							
			buffer[z] /= (normalizeDivider * multiplier);
			
		}
			
	}
	
	public void doBrightness(){
		
		initBuffer(brightness);
				
		for(int i = 0 ; i < textureColors.Length ; i++){
							
			brightness[  textureColors[i].r  ] += 1;
			
			brightness[  textureColors[i].g  ] += 1;
			
			brightness[  textureColors[i].b  ] += 1;
			
		}
		
		normalizeBuffer(brightness);
		doHistogram(brightness);
		
	}
	
	public void doLuminosity(){
/*
		initBuffer(luminosity);
		
		for(int i = 0 ; i < textureColors.Length ; i++){
			
			luminosity[textureColors[i].r] += 59;
			luminosity[textureColors[i].g] += 30;
			luminosity[textureColors[i].b] += 11;
			
		}
		
		normalizeBuffer(luminosity);
		doHistogram(luminosity);
*/
		histogramMessage1 = mainCamera.GetComponent<TonemapperDebug>().getDebugHistogram(4);
		int[] newHistogram = new int[257];
		float multiplier = 0.5f;
		for (int i = 0; i < 128; i++) {
			newHistogram[2 * i] = (int)(histogramMessage1[i] * multiplier);
			newHistogram[2*i+1] = (int)(histogramMessage1[i] * multiplier);
		}
		doHistogram(newHistogram);
	}
	
	public void doRedChannel(){
/*
		initBuffer(redChannel);
		
		for(int i = 0 ; i < textureColors.Length ; i++){
				
			redChannel[textureColors[i].r] += 1;
			
		}
		
		normalizeBuffer(redChannel);
		doHistogram(redChannel);
*/
		histogramMessage1 = mainCamera.GetComponent<TonemapperDebug>().getDebugHistogram(1);
		int[] newHistogram = new int[257];
		int multiplier = 1;
		for (int i = 0; i < 128; i++) {
			newHistogram[2 * i] = histogramMessage1[i] * multiplier;
			newHistogram[2*i+1] = histogramMessage1[i] * multiplier;
		}
		doHistogram(newHistogram);
	}
	
	public void doGreenChannel(){
/*
		initBuffer(greenChannel);
		
		for(int i = 0 ; i < textureColors.Length ; i++){

			greenChannel[textureColors[i].g] += 1;
			
		}
		
		normalizeBuffer(greenChannel);
		doHistogram(greenChannel);
*/
		histogramMessage1 = mainCamera.GetComponent<TonemapperDebug>().getDebugHistogram(2);
		int[] newHistogram = new int[257];
		int multiplier = 1;
		for (int i = 0; i < 128; i++) {
			newHistogram[2 * i] = histogramMessage1[i] * multiplier;
			newHistogram[2*i+1] = histogramMessage1[i] * multiplier;
		}
		doHistogram(newHistogram);
	}
	
	public void doBlueChannel(){
/*		
		initBuffer(blueChannel);

		for(int i = 0 ; i < textureColors.Length ; i++){
				
			blueChannel[textureColors[i].b] += 1;

		}
		
		//normalizeBuffer(blueChannel);
/*
		for (int i = 0; i < 255; i++){
			String output = "";
			output += blueChannel[i] + ",";
		}
		Debug.Log (output);
*/
		histogramMessage1 = mainCamera.GetComponent<TonemapperDebug>().getDebugHistogram(3);
		int[] newHistogram = new int[255];
		float multiplier = 100.0f / (float)histogramMessage1[130];
		float lowerIndex = ((histogramMessage1[129] / 1000000.0f) + 4) * 25.7f;
		float upperIndex = ((histogramMessage1[128] / 1000000.0f) + 4) * 25.7f;
		newHistogram[0] = 100;
		newHistogram[254] = 100;
		for (int i = 0; i < 128; i++) {
			int index = (int)(lowerIndex + (upperIndex - lowerIndex) * i / 128.0f);
			newHistogram[index] = (int)(histogramMessage1[i] * multiplier);
		}
		for (int i = 2; i < upperIndex; i++)
		{
			if (newHistogram[i] == 0)
			{
				newHistogram[i] = newHistogram[i - 1];
			}
		}
		doHistogram(newHistogram);
		
	}

	public void doClipping(){
		
		Color32[] tempBuffer = new Color32[width  * height ];
		tempBuffer = screenTexture.GetPixels32();
		
		for(int i = 0 ; i < tempBuffer.Length ; i++){

			if(tempBuffer[i].r == 0 || tempBuffer[i].g == 0 || tempBuffer[i].b == 0){

				tempBuffer[i] = colorClippingBlack;

			} else if(tempBuffer[i].r == 255 || tempBuffer[i].g == 255 || tempBuffer[i].b == 255){

				tempBuffer[i] = colorClippingWhite;

			}
			
			tempBuffer[i].a = 255;

		}

		clippingTexture.SetPixels32(tempBuffer,0);
		clippingTexture.Apply(false);

	}
							
	public void grabScreen() {

		if(screenTexture && mainCamera){
			
			mainCamera.targetTexture = renderTexture;
	
	        RenderTexture.active = renderTexture; 

			mainCamera.Render();

			screenTexture.ReadPixels( new Rect(0, 0, width , height ), 0, 0 );
			screenTexture.Apply(false);
			textureColors = screenTexture.GetPixels32();
			
			RenderTexture.active = null;

			mainCamera.targetTexture = null;
			
			switch(histogramMode){
				
			case Mode.Brightness: doBrightness();  break;
				
			case Mode.Luminosity: doLuminosity();  break;
				
			case Mode.Red: doRedChannel();  break;
				
			case Mode.Green: doGreenChannel();  break;
				
			case Mode.Blue: doBlueChannel();  break;
				
			}
	
			doClipping();
			
			Graphics.Blit(clippingTexture,renderTexture);

		}else{

			// no screen texture??
			
			initTextures();
			
			mainCamera = Camera.allCameras[mainCameraInt];

		}
		
	}

	public void changeColor(string color){
	
		switch (color){
		
		case "none":
		
			doRed = false;
			doGreen = false;
			doBlue = false;	
		
			
		break;
			
		case "red":
		
			doRed = true;
			doGreen = false;
			doBlue = false;	
			
		break;
				
		case "green":
		
			doRed = false;
			doGreen = true;
			doBlue = false;	
					
		break;
					
		case "blue":
		
			doRed = false;
			doGreen = false;
			doBlue = true;	
						
		break;
		
		
		}
	
	
	}
	
	public void setMode(int modeInt){
		
		switch(modeInt){
		
			case 0:	
			
			histogramMode = Mode.Brightness;  
			
			normalizeDivider = 60; 

			changeColor("none");
			
			break;
				
			case 1:	
			
			histogramMode = Mode.Luminosity;    
			
			normalizeDivider = 2000; 

			changeColor("none");
			
			break;
				
			case 2:	
			
			histogramMode = Mode.Red;    
			
			normalizeDivider = 10; 

			changeColor("red");
			
			break;
				
			case 3:	
			
			histogramMode = Mode.Green;    
			
			normalizeDivider = 10;

			changeColor("green"); 
			
			break;
			
			case 4:	
			
			histogramMode = Mode.Blue;    
			
			normalizeDivider = 10; 

			changeColor("blue");
			
			break;
				
		}
		
	}
	

} // class definition
