﻿using System;
using UnityEngine;

namespace UnityStandardAssets.ImageEffects
{
	[RequireComponent(typeof (Camera))]
	public class Tonemapper : PostEffectsBase
	{
		// Public variables editable in the Unity Editor GUI (with default values)
		public ComputeShader computeShader;
		public float weberConstant = 0.06f;
		public float justNoticableDeltaResponse = 0.013f;
		public float responseExponent = 0.9f;
		public float displayMaxLuminance = 400.0f;
		public float displayMinLuminance = 1.0f;
		public float partialAdaptationTime = 0.333f;

		// Variables with underscore are not meant to be edited manually:
		// These are public for debugging purposes
		public float _unbleachedPigmentCones = 1.0f;
		public float _unbleachedPigmentRods = 1.0f;
		public float _fastNeuralAdaptationCones = 100.0f;
		public float _fastNeuralAdaptationRods = 100.0f;
		public float _slowNeuralAdaptationCones = 100.0f;
		public float _slowNeuralAdaptationRods = 100.0f;
		public float _meanLogLuminance = 0;
		public float _logLuminanceRange = 0;
		public float _minLogLuminance = 0;
		public float _maxLogLuminance = 0;
		public float _relativeAvailableLuminanceUsed = 1.0f;

		private RenderTexture renderTexture; // This texture gets rendered on screen
		private RenderTexture luminanceTexture;
		private RenderTexture chrominanceTexture1;
		private RenderTexture chrominanceTexture2;
		private ComputeBuffer reducedLuminanceBuffer;
		private ComputeBuffer histogramBuffer;

		// The following const has to be manually kept in sync with NUM_THREADS_X in file computeShader.compute
		private const int NUM_THREADS_X = 16;
		private const int NUM_THREADS_Y = NUM_THREADS_X;
		private const int NUM_HISTOGRAM_BINS = 128;

		public override bool CheckResources()
		{
			CheckSupport(false, true); // Do not need depth, do need hdr
			if (!isSupported) { ReportAutoDisable(); }
			return isSupported;
		}

		[ImageEffectTransformsToLDR]
		private void OnRenderImage(RenderTexture source, RenderTexture destination)
		{
			// Prepare variables buffers and textures
			int numGroupsX = (int)Mathf.Ceil((float)source.width / NUM_THREADS_X);
			int numGroupsY = (int)Mathf.Ceil((float)source.height / NUM_THREADS_Y);

			// To avoid weighting calculations for non-full groups at the edges of the image
			// the operator only uses full groups for histogram calculations
			int numFullGroupsX = (int)Mathf.Floor((float)source.width / NUM_THREADS_X);
			int numFullGroupsY = (int)Mathf.Floor((float)source.height / NUM_THREADS_Y);
			int numFullGroups = numFullGroupsX * numFullGroupsY;

			// Temporary Textures let Unity handle memory more efficiently, but potential source for bugs
			renderTexture = RenderTexture.GetTemporary(source.width, source.height, 0, RenderTextureFormat.ARGBFloat);
			renderTexture.enableRandomWrite = true;
			renderTexture.Create();
			luminanceTexture = RenderTexture.GetTemporary(source.width, source.height, 0, RenderTextureFormat.RFloat);
			luminanceTexture.enableRandomWrite = true;
			luminanceTexture.Create();
			chrominanceTexture1 = RenderTexture.GetTemporary(source.width, source.height, 0, RenderTextureFormat.RFloat);
			chrominanceTexture1.enableRandomWrite = true;
			chrominanceTexture1.Create();
			chrominanceTexture2 = RenderTexture.GetTemporary(source.width, source.height, 0, RenderTextureFormat.RFloat);
			chrominanceTexture2.enableRandomWrite = true;
			chrominanceTexture2.Create();

			reducedLuminanceBuffer = new ComputeBuffer(numFullGroups, 4);
			histogramBuffer = new ComputeBuffer(NUM_HISTOGRAM_BINS, 4);

			// Prepare the color space conversion an decimation kernels
			computeShader.SetInt("numCols", source.width);
			computeShader.SetInt("numRows", source.height);
			computeShader.SetInt("numFullGroupsX", numFullGroupsX);
			computeShader.SetTexture(0, "inTexture", source);
			computeShader.SetTexture(0, "luminanceTexture", luminanceTexture);
			computeShader.SetTexture(0, "chrominanceTexture1", chrominanceTexture1);
			computeShader.SetTexture(0, "chrominanceTexture2", chrominanceTexture2);
			computeShader.SetTexture(1, "luminanceTexture", luminanceTexture);
			computeShader.SetBuffer(1, "reducedLuminanceBuffer", reducedLuminanceBuffer);

			// Kernel 0: Convert from rgb to SYZ/Yxy, take log10
			computeShader.Dispatch(0, numGroupsX, numGroupsY, 1);

			// Kernel 1:Decimate log luminance data by a factor of ca. 64
			computeShader.Dispatch(1, numFullGroupsX, numFullGroupsY, 1);

			float[] readReduced = new float[numFullGroups];
			reducedLuminanceBuffer.GetData(readReduced);

			// Find highest and lowest and mean Luminance Values:
			_maxLogLuminance = -0x10000000;
			_minLogLuminance = 0x10000000;
			_meanLogLuminance = 0.0f;
			for (int i = 0; i < numFullGroups; i++)
			{
				if (readReduced[i] < _minLogLuminance && readReduced[i] != 0)
				{
					_minLogLuminance = readReduced[i];
				}
				if (readReduced[i] > _maxLogLuminance)
				{
					_maxLogLuminance = readReduced[i];
				}
				_meanLogLuminance += readReduced[i] / numFullGroups;
			}

			// Clip at lower threshold for human vision:
			if (_minLogLuminance < -4.0f)
			{
				_minLogLuminance = -4.0f;
			}

			// Fill histogram bins
			_logLuminanceRange = _maxLogLuminance - _minLogLuminance;
			int[] histogramBins = new int[NUM_HISTOGRAM_BINS];
			for (int i = 0; i < numFullGroups; i++)
			{
				int binIndex = (int)Mathf.Max(0, Mathf.Min(NUM_HISTOGRAM_BINS - 1,
						Mathf.Ceil((readReduced[i] - _minLogLuminance) /
						(_logLuminanceRange / (float)NUM_HISTOGRAM_BINS))));
				histogramBins[binIndex]++;
			}

			updateAdaptationVariables(_meanLogLuminance, Time.deltaTime);

			// Check for Low Dynamic Range condition:
			float displayLuminanceRange = Mathf.Log10(displayMaxLuminance) - Mathf.Log10(displayMinLuminance);
			int intermediateSum = 0;
			float worldLuminancePerBin = _logLuminanceRange / NUM_HISTOGRAM_BINS;
			int[] ceilings = new int[NUM_HISTOGRAM_BINS];
			for (int bin = 0; bin < NUM_HISTOGRAM_BINS; bin++)
			{
				float worldBinLuminance = worldLuminancePerBin * bin + _minLogLuminance;
				float tmpPart = (numFullGroups / (float)NUM_HISTOGRAM_BINS) * (weberConstant /
						(calculateTVIA(worldBinLuminance) /
						Mathf.Pow (10.0f, worldBinLuminance)));
				intermediateSum += (int)Mathf.Round(tmpPart);
				ceilings[bin] = (int)Mathf.Round(tmpPart * _logLuminanceRange / displayLuminanceRange);
			}

			float relativeDisplayLuminanceRangeReduction = 0;
			float worldLuminanceBump = 0;
			_relativeAvailableLuminanceUsed = 1.0f;
			// This statement checks for LDR
			if (intermediateSum * (_logLuminanceRange / displayLuminanceRange) < numFullGroups)
			{
				float oldDisplayLuminanceRange = displayLuminanceRange;
				displayLuminanceRange = intermediateSum * (_logLuminanceRange / numFullGroups);
				for (int bin = 0; bin < NUM_HISTOGRAM_BINS; bin++)
				{
					// Rounding errors do not make a difference here (tested)
					ceilings[bin] = (int)Mathf.Round((ceilings[bin] * oldDisplayLuminanceRange) / displayLuminanceRange);
				}

				// Choose what part of screen luminance range should be used
				relativeDisplayLuminanceRangeReduction = 1 - (displayLuminanceRange / oldDisplayLuminanceRange);
				if (Mathf.Log10(displayMaxLuminance) < _maxLogLuminance)
				{
					worldLuminanceBump = relativeDisplayLuminanceRangeReduction;
				}
				else if (Mathf.Log10(displayMinLuminance) < _minLogLuminance)
				{
					worldLuminanceBump = _minLogLuminance / oldDisplayLuminanceRange;
					if (worldLuminanceBump > relativeDisplayLuminanceRangeReduction)
					{
						worldLuminanceBump = relativeDisplayLuminanceRangeReduction;
					}
				}

				// In LDR mode, the trimmed histogram equals the ceilings
				Array.Copy(ceilings, histogramBins, NUM_HISTOGRAM_BINS);
			}
			else // not LDR case
			{
				// Trimm histogram:
				int trimmings = 0;
				int sumOfUntrimmedValues = 0;
				int sumOfEmptyBinCeilings = 0;
				for (int bin = 0; bin < NUM_HISTOGRAM_BINS; bin++)
				{
					if (histogramBins[bin] >= ceilings[bin])
					{
						trimmings += (histogramBins[bin] - ceilings[bin]);
						histogramBins[bin] = ceilings[bin];
					}
					else if (histogramBins[bin] == 0)
					{
						sumOfEmptyBinCeilings += ceilings[bin];
					}
					else
					{
						sumOfUntrimmedValues += histogramBins[bin];
					}
				}
				
				// Redistribute as many trimmings as possible proportionally
				bool hasSpaceInNonemptyBins = true;
				while (trimmings > 0 && hasSpaceInNonemptyBins)
				{
					if (hasSpaceInNonemptyBins)
					{
						hasSpaceInNonemptyBins = false;
						for (int bin = 0; bin < NUM_HISTOGRAM_BINS; bin++)
						{
							if (histogramBins[bin] > 0 && histogramBins[bin] < ceilings[bin] && trimmings > 0)
							{
								int proportionalTrimmings = (int)Mathf.Ceil((trimmings * histogramBins[bin] /
										(float)sumOfUntrimmedValues));
								if (histogramBins[bin] + proportionalTrimmings >= ceilings[bin])
								{
									trimmings -= (ceilings[bin] - histogramBins[bin]);
									sumOfUntrimmedValues -= histogramBins[bin];
									histogramBins[bin] = ceilings[bin];
								}
								else
								{
									hasSpaceInNonemptyBins = true;
									trimmings -= proportionalTrimmings;
									histogramBins[bin] += proportionalTrimmings;
									sumOfUntrimmedValues += proportionalTrimmings;
								}
							}
						}
					}
				}

				// Distribute the rest of the trimmings evenly among empty bins
				if (trimmings > 0 && sumOfEmptyBinCeilings > 0)
				{
					for (int bin = 0; bin < NUM_HISTOGRAM_BINS; bin++)
					{
						if (histogramBins[bin] == 0)
						{
							histogramBins[bin] = (int)Mathf.Round(trimmings * ceilings[bin] / (float)sumOfEmptyBinCeilings);
						}
					}
				}
			}

			// Inklusive sum scan on histogram to get cumulative distribution
			for (int i = 1; i < NUM_HISTOGRAM_BINS; i++)
			{
				histogramBins[i] += histogramBins[i - 1];
				// Reduce dynamic range used in case of LDR-scene:
				histogramBins[i - 1] = (int)Mathf.Round (histogramBins[i - 1] * (1 -
						relativeDisplayLuminanceRangeReduction) + numFullGroups * worldLuminanceBump);
			}

			// In case of LDR, compress values so that not all of the screen dynamic range is used:
			histogramBins[NUM_HISTOGRAM_BINS - 1] = (int)Mathf.Round (histogramBins[NUM_HISTOGRAM_BINS - 1] * (1 -
					relativeDisplayLuminanceRangeReduction) + numFullGroups * worldLuminanceBump);

			// Prepare tonemapping kernel
			histogramBuffer.SetData(histogramBins);
			computeShader.SetInt("numGroups", numFullGroups);
			computeShader.SetFloat("logLuminanceRange", _logLuminanceRange);
			computeShader.SetFloat("minLogLuminance", _minLogLuminance);
			computeShader.SetBuffer(2, "histogramBuffer", histogramBuffer);
			computeShader.SetTexture(2, "luminanceTexture", luminanceTexture);
			computeShader.SetTexture(2, "chrominanceTexture1", chrominanceTexture1);
			computeShader.SetTexture(2, "chrominanceTexture2", chrominanceTexture2);
			computeShader.SetTexture(2, "outTexture", renderTexture);

			// Kernel 2: Apply tonemapping and convert back to rgb
			computeShader.Dispatch(2, numGroupsX, numGroupsY, 1);

			// Paint!
			Graphics.Blit(renderTexture, destination);

			// Cleanup
			reducedLuminanceBuffer.Dispose();
			histogramBuffer.Dispose();
			RenderTexture.ReleaseTemporary(luminanceTexture);
			RenderTexture.ReleaseTemporary(chrominanceTexture1);
			RenderTexture.ReleaseTemporary(chrominanceTexture2);
			RenderTexture.ReleaseTemporary(renderTexture);
		}

		private float calculateUnbleachedPigmentCones(float luminance, float seconds)
		{
			float targetUnbleachedPigment = 10000.0f / (10000.0f + luminance);
			return targetUnbleachedPigment + (_unbleachedPigmentCones - targetUnbleachedPigment) *
					Mathf.Exp(-seconds / (110.0f * targetUnbleachedPigment));
		}

		private float calculateUnbleachedPigmentRods(float luminance, float seconds)
		{
			float targetUnbleachedPigment = 10000.0f / (10000.0f + luminance);
			return targetUnbleachedPigment + (_unbleachedPigmentRods - targetUnbleachedPigment) *
					Mathf.Exp(-seconds / (400.0f * targetUnbleachedPigment));
		}

		private float calculateFastNeuralAdaptationCones(float luminance, float seconds)
		{
			return luminance + (_fastNeuralAdaptationCones - luminance) *
					Mathf.Exp(-seconds / 0.08f);
		}

		private float calculateFastNeuralAdaptationRods(float luminance, float seconds)
		{
			return luminance + (_fastNeuralAdaptationRods - luminance) *
					Mathf.Exp(-seconds / 0.15f);
		}

		private float calculateSlowNeuralAdaptationCones(float luminance, float seconds)
		{
			return luminance + (_slowNeuralAdaptationCones - luminance) *
					Mathf.Exp(-seconds / 30.0f);
		}

		private float calculateSlowNeuralAdaptationRods(float luminance, float seconds)
		{
			return luminance + (_slowNeuralAdaptationRods - luminance) *
					Mathf.Exp(-seconds / 60.0f);
		}

		private void updateAdaptationVariables(float logLuminance, float deltaTime)
		{
			float luminance = Mathf.Pow(10.0f, logLuminance);
			_unbleachedPigmentCones = calculateUnbleachedPigmentCones(luminance, deltaTime);
			_unbleachedPigmentRods = calculateUnbleachedPigmentRods(luminance, deltaTime);
			_fastNeuralAdaptationCones = calculateFastNeuralAdaptationCones(luminance, deltaTime);
			_fastNeuralAdaptationRods = calculateFastNeuralAdaptationRods(luminance, deltaTime);
			_slowNeuralAdaptationCones = calculateSlowNeuralAdaptationCones(luminance, deltaTime);
			_slowNeuralAdaptationRods = calculateSlowNeuralAdaptationRods(luminance, deltaTime);
		}

		private float calculateAdaptationStateCones(float luminance)
		{
			float tmpUnbleachedPigmentCones = calculateUnbleachedPigmentCones(luminance, partialAdaptationTime);
			float tmpFastNeuralAdaptationCones = calculateFastNeuralAdaptationCones(luminance, partialAdaptationTime);
			float tmpSlowNeuralAdaptationCones = calculateSlowNeuralAdaptationCones(luminance, partialAdaptationTime);

			float pigmentBleachingAdaptationCones = 1.0f / tmpUnbleachedPigmentCones;
			float luminancePower = Mathf.Pow(tmpFastNeuralAdaptationCones, 0.6406f);
			float logFastNeuralAdaptationCones = (2.027f * luminancePower) / (luminancePower + 3.10361422f) + 0.01711f;
			luminancePower = Mathf.Pow(tmpSlowNeuralAdaptationCones, 0.8471f);
			float logSlowNeuralAdaptationCones = (1.929f * luminancePower) / (luminancePower + 361.8662189f) + 0.0182f;

			return pigmentBleachingAdaptationCones *
					Mathf.Pow(10.0f, logFastNeuralAdaptationCones) *
					Mathf.Pow(10.0f, logSlowNeuralAdaptationCones);
		}

		private float calculateAdaptationStateRods(float luminance)
		{
			float tmpUnbleachedPigmentRods = calculateUnbleachedPigmentRods(luminance, partialAdaptationTime);
			float tmpFastNeuralAdaptationRods = calculateFastNeuralAdaptationRods(luminance, partialAdaptationTime);
			float tmpSlowNeuralAdaptationRods = calculateSlowNeuralAdaptationRods(luminance, partialAdaptationTime);
			
			float pigmentBleachingAdaptationRods = 1.0f / tmpUnbleachedPigmentRods;
			float luminancePower = Mathf.Pow(tmpFastNeuralAdaptationRods, 0.3604f);
			float logFastNeuralAdaptationRods = (2.311f * luminancePower) / (luminancePower + 0.175979698f) - 2.749f;
			luminancePower = Mathf.Pow(tmpSlowNeuralAdaptationRods, 0.9524f);
			float logSlowNeuralAdaptationRods = (1.735f * luminancePower) / (luminancePower + 1.262223352f) + 0.005684f;
			
			return pigmentBleachingAdaptationRods *
					Mathf.Pow(10.0f, logFastNeuralAdaptationRods) *
					Mathf.Pow(10.0f, logSlowNeuralAdaptationRods);
		}

		private float calculateReceptorResponse(float luminance, float adaptation)
		{
			float responseLuminancePower = Mathf.Pow(luminance, responseExponent);
			float adaptationPower = Mathf.Pow(adaptation, responseExponent);
			return responseLuminancePower / (responseLuminancePower + adaptationPower);
		}

		// Threshold vs intensity and adaptation function
		private float calculateTVIA(float logLuminance)
		{
			float luminance = Mathf.Pow(10.0f, logLuminance);
			float lowerEndMesopicRange = -0.0f;
			float upperEndMesopicRange = 4.0f;

			float thresholdCones;
			float adaptationStateCones = calculateAdaptationStateCones(luminance);
			float responseCones = calculateReceptorResponse(luminance, adaptationStateCones);
			if (responseCones + justNoticableDeltaResponse >= 1.0f)
			{
				thresholdCones = 0x10000000;
			}
			else
			{
				float tmpFraction = (responseCones + justNoticableDeltaResponse) /
						(1.0f - (responseCones + justNoticableDeltaResponse));
				thresholdCones = adaptationStateCones * Mathf.Pow(tmpFraction, 1.0f / responseExponent) - luminance;
			}

			float thresholdRods;
			float adaptationStateRods = calculateAdaptationStateRods(luminance);
			float responseRods = calculateReceptorResponse(luminance, adaptationStateRods);
			if (responseRods + justNoticableDeltaResponse >= 1.0f)
			{
				thresholdRods = 0x10000000;
			}
			else
			{
				float tmpFraction = (responseRods + justNoticableDeltaResponse) /
						(1.0f - (responseRods + justNoticableDeltaResponse));
				thresholdRods = adaptationStateRods * Mathf.Pow(tmpFraction, 1.0f / responseExponent) - luminance;
			}

			// Temporary workaround to account for rods not working above a certain range
			float mesopicRange = upperEndMesopicRange - lowerEndMesopicRange;
			float tmpLuminance = logLuminance - lowerEndMesopicRange;
			float balanceFactor = Mathf.Min(Mathf.Max(0.0f, tmpLuminance / mesopicRange), 1.0f);
			thresholdRods = Mathf.Pow(10, Mathf.Log10(thresholdRods) + 3.0f * balanceFactor);

			return thresholdRods < thresholdCones ? thresholdRods : thresholdCones;
		}
	}
}
