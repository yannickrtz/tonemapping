﻿using System;
using System.Reflection;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

[ExecuteInEditMode]
public class TonemapperDebugInspector : MonoBehaviour
{
	public TonemapperDebug target;
	public GUIStyle histoStyle;
	public int histogramNumber = 4;
	public float adaptationVariable = 2f;
	public bool showLessVariables = true;

	private int[] histogramMessage1;

	private float[] maxes = {0.2f, 0.03f, 1.1f, 2000f, 20f,  1f, 80f};
	private float[] mins = {0.001f, 0.0001f, 0.5f, 40f, 0.01f, 0f, 0.1f};
	private string[] inputs = {"0.06", "0.013", "0.7", "400", "1", "0.1", "5"};
	private float[] defaults = {0.06f, 0.013f, 0.7f, 400f, 1f, 0.1f, 5f};

	void Init()
	{
		// Init stuff
	}

	void OnGUI()
	{
		GUILayout.BeginArea(new Rect(10, 10, 410, 1000));
		GUILayout.BeginVertical("box");

		GUILayout.Label("Implementation of an HDR Tone Mapping Operator Based on Human Perception by Yannick Rietz, 2015");
		GUILayout.Label("Hold G to move view with the mouse, D to zoom in, F to zoom out. " +
				"Modify values by using the sliders or number input fields.");

		GUILayout.BeginHorizontal();
		String moreButtonText = "Show less variables";
		if (showLessVariables)
		{
			moreButtonText = "Show more variables";
		}
		if (GUILayout.Button(moreButtonText))
		{
			showLessVariables = !showLessVariables;
		}
		bool doReset = false;
		if (GUILayout.Button("Reset all to default"))
		{
			doReset = true;
		}
		if (GUILayout.Button("Toggle linear mapping"))
		{
			Camera.main.GetComponent<TonemapperDebug>().toggleLinearMode();
		}
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Fully adapt"))
		{
			Camera.main.GetComponent<TonemapperDebug>().fullyAdapt();
		}
		if (GUILayout.Button("Adapt to variable"))
		{
			Camera.main.GetComponent<TonemapperDebug>().fullyAdapt(adaptationVariable);
		}
		if (GUILayout.Button("Toggle Block View"))
		{
			Camera.main.GetComponent<TonemapperDebug>().toggleLuminancesView();
		}
		GUILayout.EndHorizontal();


		Type type = typeof(TonemapperDebug);
		FieldInfo[] fields = type.GetFields();
		int index = 0;
		foreach (FieldInfo field in fields) {
			if (field.FieldType == typeof(float) && (field.Name[0] != '_' || !showLessVariables))
			{
				GUILayout.BeginHorizontal();
				GUILayout.Label(field.Name, GUILayout.Width(200f));
				if (field.Name[0] != '_')
				{
					if (doReset)
					{
						field.SetValue(target, defaults[index]);
					}

					field.SetValue(
						target,
						GUILayout.HorizontalSlider(
							(float)field.GetValue(target),
							mins[index],
							maxes[index],
							GUILayout.Width(100f)
						)
					);
					float parseResult;
					Single.TryParse(inputs[index], out parseResult);
					if ((float)field.GetValue(target) != parseResult)
					{
						inputs[index] = String.Format("{0:G4}", field.GetValue(target));
					}
					string newInput = GUILayout.TextArea(inputs[index], GUILayout.Width(50f));
					bool isValidInput = Single.TryParse(newInput, out parseResult);
					if (isValidInput)
					{
						field.SetValue(target, parseResult);
						inputs[index] = newInput;
					}
					if (GUILayout.Button("reset"))
					{
						field.SetValue(target, defaults[index]);
						inputs[index] = defaults[index].ToString();
					}
				}
				else
				{
					GUILayout.Label(
						((float)field.GetValue(target)).ToString("R")
					);
				}
				/*
				GUILayout.Label(
					((float)field.GetValue(target)).ToString("R")
				);
				*/
				GUILayout.EndHorizontal();
				index += 1;
			}
		}

		GUILayout.BeginHorizontal();
		GUILayout.Label("Variable to adapt to", GUILayout.Width(200f));
		adaptationVariable = GUILayout.HorizontalSlider(adaptationVariable,
				-4f, 6f, GUILayout.Width(100f));
		GUILayout.Label(adaptationVariable.ToString("R"));
		GUILayout.EndHorizontal();

		int[] histogramMessage1 = Camera.main.GetComponent<TonemapperDebug>().getDebugHistogram(histogramNumber);

		int newHistogramWidth = 385;
		int newHistogramHeight = 170;
		int[] newHistogram = new int[newHistogramWidth];
		float highestValue = 0;
/*
		for (int bin = 0; bin < 128; bin++)
		{
			if (histogramMessage1[bin] > highestValue)
			{
				highestValue = histogramMessage1[bin];
			}
		}
*/
		highestValue = histogramMessage1[130];
		float multiplier = newHistogramHeight / highestValue;
		float lowerIndex = ((histogramMessage1[129] / 1000000.0f) + 4) * newHistogramWidth / 10;
		float upperIndex = ((histogramMessage1[128] / 1000000.0f) + 4) * newHistogramWidth / 10;
		upperIndex = upperIndex >= newHistogramWidth ? newHistogramWidth - 1 : upperIndex;
		for (int i = 0; i < 128; i++) {
			int ind = (int)(lowerIndex + (upperIndex - lowerIndex) * i / 128.0f);
			if (ind < newHistogramWidth)
			{
				newHistogram[ind] = (int)(histogramMessage1[i] * multiplier);
			}
		}

		if (histogramNumber == 3 || histogramNumber == 1)
		{
			for (int i = 1; i < upperIndex; i++)
			{
				if (newHistogram[i] == 0)
				{
					newHistogram[i] = newHistogram[i - 1];
				}
			}	
		}

		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Input Histo"))
		{
			histogramNumber = 4;
		}
		if (GUILayout.Button("Trimmed Histo"))
		{
			histogramNumber = 2;
		}
		if (GUILayout.Button("Ceilings Graph"))
		{
			histogramNumber = 1;
		}
		if (GUILayout.Button("Mapping Graph"))
		{
			histogramNumber = 3;
		}
		GUILayout.EndHorizontal();

		GUILayout.BeginVertical("box");
		GUILayout.BeginHorizontal("box");
		for (int bin = 0; bin < newHistogramWidth; bin++)
		{
			if (newHistogram[bin] > 0)
			{
				if (histogramNumber == 3 || histogramNumber == 1)
				{
					GUILayout.BeginVertical(GUILayout.Height(newHistogramHeight - newHistogram[bin]), GUILayout.Width(1f));
					GUILayout.FlexibleSpace();
					GUILayout.Box("", histoStyle, GUILayout.Height(2f), GUILayout.Width(1f));
				}
				else
				{
					GUILayout.BeginVertical(GUILayout.Height(newHistogramHeight), GUILayout.Width(1f));
					GUILayout.FlexibleSpace();
					GUILayout.Box("", histoStyle, GUILayout.Height(newHistogram[bin]), GUILayout.Width(1f));
				}
			}
			else
			{
				GUILayout.BeginVertical(GUILayout.Height(newHistogramHeight), GUILayout.Width(1f));
				GUILayout.FlexibleSpace();
			}
			GUILayout.EndVertical();
		}
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Label("10^-4 cd/m2");
		GUILayout.Space(245);
		GUILayout.Label("10^6 cd/m2");
		
		GUILayout.EndVertical();
		GUILayout.EndVertical();
		GUILayout.EndArea();
	}
}